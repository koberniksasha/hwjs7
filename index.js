"use strict";

/*    1.   Опишіть своїми словами як працює метод forEach.
        
            перебирає елементи масиву, нічого не повертає
            [2, 4, 5, 6, 7].forEach(item => console.log(item))

      2.    Як очистити масив?
            arr.splice(0,arr.lenght) // arr.lenght = 0
            


      3.    Як можна перевірити, що та чи інша змінна є масивом?
            console.log(Array.isArray(myArray));
                 */

function filterBy(arr, dataType) {
  let newFilterArr = arr.filter((item) => typeof item !== dataType);
  return newFilterArr;
}

let newUser = ["hello", "world", 23, "23", null];
console.log(filterBy(newUser, "string"));
